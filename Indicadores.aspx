﻿<%@ Page Title="Indicadores" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Indicadores.aspx.cs" Inherits="SICRER_Web_Application.Indicadores"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="Scripts/revisiones.js"></script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li ><a href="ConfigurarAdministradores.aspx">Usuarios</a></li>
            <li ><a href="Configurar_Semestres.aspx">Semestres</a></li>
            <li class="active"><a href="Indicadores.aspx">Indicadores</a></li>
            <li><a href="Carreras.aspx">Carreras</a></li>
             <li id="confEmail" runat="server" visible="false"><a href="Configurar_Correo.aspx">Correo</a></li>
        </ul>
    </div>

    <div class="col-md-9 space-up">
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <div class="form-inline">
            <asp:DropDownList id="ddTp_ind" runat="server" CssClass="selectpicker form-control" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddTp_ind_SelectedIndexChanged">
                <asp:ListItem Text="Cocina" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Cuarto" Value="2"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList id="ddBajaAlta" runat="server" CssClass="selectpicker form-control" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddTp_ind_SelectedIndexChanged">
                <asp:ListItem Text="Dados de Alta" Value="false" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Dados de Baja" Value="true"></asp:ListItem>
            </asp:DropDownList>
            <asp:LinkButton id="linkNuevo" runat="server" Text=" Crear Nuevo " CssClass="btn btn-primary" OnClientClick="mostrarFormRegistrarInd(); return false;"/>
        </div>

        <div id="formRegistrarInd" class="space-up" style="display:none;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Registrar Indicador</div>
                </div>
                <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lbNombreInd" runat="server" Text="Nombre " CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:TextBox ID="tbNombreInd" runat="server" CssClass="form-control col-sm-4"></asp:TextBox>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="lbPorcenInd" runat="server" Text="Porcentaje" CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:TextBox ID="tbPorcenInd" runat="server" CssClass="form-control col-sm-4"></asp:TextBox>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8" style="align-content:center">
                            <asp:Button ID="btnRegistrarInd" runat="server" Text="Agregar" CssClass="btn btn-success" OnClick="btnRegistrarInd_Click"/>
                            <asp:Button ID="btnCancelarRegistroInd" runat="server" Text="Cancelar" CssClass="btn btn-primary" OnClientClick="ocultarFormRegistrarInd();" OnClick="btnCancelarRegistroInd_Click"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group space-up">
            <asp:Label ID="lbMsjNoInd" runat="server" Text="" ForeColor="Black" Font-Bold="true" Font-Italic="true" Font-Size="Medium" Visible="false"></asp:Label>
        </div>

        <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:GridView id="gvInd" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover space-up" 
                    OnRowEditing="gvInd_RowEditing" 
                    OnRowCancelingEdit="gvInd_RowCancelingEdit" 
                    OnRowDeleting="gvInd_RowDeleting" 
                    OnRowUpdating="gvInd_RowUpdating">

                    <Columns>
                        <asp:BoundField  DataField="Indicador" HeaderText="Rubros de Calificación"/>
                        <asp:BoundField  DataField="Porcentaje" HeaderText="Porcentaje Asignado"/>
                        <asp:CommandField ShowEditButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-warning" CancelText="Cancelar" ItemStyle-HorizontalAlign="Center">
                        </asp:CommandField>
                        <asp:CommandField ShowDeleteButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-danger" DeleteText="Dar Baja" ItemStyle-HorizontalAlign="Center">
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>

                <asp:Label ID="lbtextoInd" runat="server" Text="Suma Total Indicadores: " Font-Bold="true" Font-Size="Medium"> </asp:Label>
                <asp:Label ID="lbPorcentageInd" runat="server" Text="  " Font-Bold="true" Font-Size="Medium"> </asp:Label>
                <asp:Image ID="ImgInfoInd" runat="server" ImageUrl="~/img/information_icon2.png" ToolTip="La suma de los porcentajes debe ser 100" CssClass="space-down"/>

                <div class="form-group space-up pull-right">
                    <asp:Button ID="btnGuardarCambiosInd" runat="server" Text="Guardar Cambios" Visible="false" CssClass="btn btn-success" OnClick="btnGuardarCambiosInd_Click"/>
                    <asp:Button ID="btnCancelarCambiosInd" runat="server" Text="Cancelar Cambios" Visible="false" CssClass="btn btn-primary" OnClick="btnCancelarCambiosInd_Click"/>
                </div>
           </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnGuardarCambiosInd"/>
                <asp:PostBackTrigger ControlID="btnCancelarCambiosInd"/>
            </Triggers>
        </asp:UpdatePanel>

        <asp:GridView ID="gvIndDeBaja" runat="server" 
            AutoGenerateColumns="false"
            CssClass="table table-bordered table-striped table-hover table-condensed"
            OnRowCommand="gvIndDeBaja_RowCommand">
            <Columns>
                <asp:BoundField  DataField="Indicador" HeaderText="Rubros de Calificación"/>
                <asp:BoundField  DataField="Porcentaje" HeaderText="Porcentaje Asignado"/>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Button ID="btnDarDeAlta" runat="server" 
                            CommandName="Alta" 
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                            Text="Dar de Alta"
                            CssClass="btn btn-warning"
                            />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
       </asp:GridView>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
