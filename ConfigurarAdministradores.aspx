﻿<%@ Page Title="Configurar Usuarios" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigurarAdministradores.aspx.cs" Inherits="SICRER_Web_Application.ConfigurarAdministradores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="HiddenField1" Value="1" runat="server" />
    <script>
        function volver() {
            history.back();
        }
        
        var options = {
            trigger: 'hover focus'
        }
        $(function () {
            $('[data-toggle="popover"]').popover(options);
            debugger;
        })

        $(function () {
            $('[data-toggle="tooltip"]').tooltip({ fade: 250 });
            debugger;
        })

        var hiddenInput = document.getElementById('<%=HiddenField1.ClientID %>');


        function cargandoPagina() {
            var valor = hiddenInput.value;

            if (valor == 1) {

                selectorAdministrador(1);
                esconderColumnasAdministrador();
                habilitarRequest();

            }
            else if (valor == 2) {
                selectorRevisor(1);
                esconderColumnasRevisor();
                habilitarRequest();

            }
            else if (valor == 3) {
                selectorCoordinador(1);
                esconderColumnasCoordinador();
                habilitarRequest();
            }

        }


        $(document).ready(function () {


            cargandoPagina();
            inhabilitarRequest();
            $("#tabla").css("display", "block");
            debugger;
            $('#qgvAdministradores tr').each(function () {

                $(this).find('th').each(function () {
                    var columna = $(this).index();
                    if (columna == 0) {
                        $(this).css("display", "none");
                    }
                });
                $(this).find('td').each(function () {
                    var columna = $(this).index();
                    if (columna == 0) {
                        $(this).css("display", "none");
                    }

                });
            });



        });

        function limpiarCampos() {
            document.getElementById('<%=tbNombre1.ClientID %>').value = "";
            document.getElementById('<%=tbEmail1.ClientID %>').value = "";
            document.getElementById('<%=tbCarne1.ClientID %>').value = "";
            document.getElementById('<%=tbTelefono1.ClientID %>').value = "";
            document.getElementById('<%=tbUsuario1.ClientID %>').value = "";
            document.getElementById('<%=tbContrasena1.ClientID %>').value = "";
            document.getElementById('<%=tbRepetirContrasena1.ClientID %>').value = "";

            document.getElementById('<%=RequiredFieldValidator1.ClientID %>').style.display = "none";
            document.getElementById('<%=RequiredFieldValidator2.ClientID %>').style.display = "none";
            document.getElementById('<%=RequiredFieldValidator3.ClientID %>').style.display = "none";
            document.getElementById('<%=RequiredFieldValidator4.ClientID %>').style.display = "none";
            document.getElementById('<%=RequiredFieldValidator5.ClientID %>').style.display = "none";
            document.getElementById('<%=RequiredFieldValidator6.ClientID %>').style.display = "none";





        }

        function inhabilitarRequest() {
            document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
        }

        function habilitarRequest() {
            if (hiddenInput.value == 1) {
                document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;

                document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
            }
            else if (hiddenInput.value == 2) {
                document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
                document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;

            }
            else if (hiddenInput.value == 3) {
                document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
                document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
                debugger;
            }
            debugger;
        }

        function ocultarFormulario() {

            $("#formulario").css("display", "none");
            document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;


        }
        function mostrarFormulario() {

            $("#formulario").css("display", "block");
            debugger;
            habilitarRequest();


        }
        function SetDataField(data) {
            var hiddenInput = document.getElementById('<%=HiddenField1.ClientID %>');
            hiddenInput.value = data;


        }
        function selectorRevisor(indice) {
            SetDataField(2);
            habilitarRequest();

            if (indice == 0) {
                '<%=updateButtons()%>'
                limpiarCampos();

                document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
                document.getElementById('<%=Button1.ClientID %>').text = "Registrar";

            }
            $("#tipoUsuario").text("Registro de Revisor");

            $("#divTelefono").css("display", "none");
            $("#divEmail").css("display", "none");
            $("#divCarne").css("display", "none");
            document.getElementById('<%=tbNombre1.ClientID %>').disabled = false;
            document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Revisor";

            $("#tipoRev").addClass("active");
            $("#tipoAdmin").removeClass("active");
            $("#tipoCor").removeClass("active");




            esconderColumnasRevisor();

        }
        function selectorAdministrador(indice) {
            SetDataField(1);
            habilitarRequest();

            debugger;
            if (indice == 0) {
                limpiarCampos();
                debugger;
                document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
                document.getElementById('<%=Button1.ClientID %>').text = "Registrar";
            }
            $("#tipoUsuario").text("Registro de Administrador");

            $("#divTelefono").css("display", "block");
            $("#divEmail").css("display", "block");
            $("#divCarne").css("display", "none");
            document.getElementById('<%=tbNombre1.ClientID %>').disabled = false;
            document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Administrador";

            $("#tipoRev").removeClass("active");
            $("#tipoAdmin").addClass("active");
            $("#tipoCor").removeClass("active");




            esconderColumnasAdministrador();


        }
        function selectorCoordinador(indice) {
            SetDataField(3);
            habilitarRequest();

            if (indice == 0) {

                limpiarCampos();

                document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
                document.getElementById('<%=Button1.ClientID %>').text = "Registrar";


            }
            $("#tipoUsuario").text("Registro de Coordinador");
            $("#divTelefono").css("display", "none");
            $("#divEmail").css("display", "none");
            $("#divCarne").css("display", "block");

            $('<%=tbNombre1.ClientID %>').attr("placeholder", "Estudiante");
            $("#tipoRev").removeClass("active");
            $("#tipoAdmin").removeClass("active");

            $("#tipoCor").addClass("active");

            document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Coordinador";
            document.getElementById('<%=tbNombre1.ClientID %>').disabled = true;

            esconderColumnasCoordinador();



        }

        function esconderColumnasCoordinador() {
            var gridrows = document.getElementById('MainContent_gvAdministradores').rows;

            for (i = 0; i < gridrows.length; i++) {
                gridrows[i].cells[1].style.display = "none";
                gridrows[i].cells[2].style.display = "none";
                gridrows[i].cells[3].style.display = "none";
                gridrows[i].cells[4].style.display = "none";
            }

            $('#MainContent_gvAdministradores').each(function () {
                $(this).find('tr').each(function () {
                    var admin = $(this).find("select option:selected").text();

                    if (admin == "Administrador" || admin == "Revisor") {
                        $(this).css("display", "none");
                    }
                    else if (admin == "Coordinador") {
                        $(this).css("display", "");
                    }

                });
            });
            return false;

        }
        function esconderColumnasAdministrador() {
            var gridrows = document.getElementById('MainContent_gvAdministradores').rows;

            for (i = 0; i < gridrows.length; i++) {
                gridrows[i].cells[1].style.display = "";
                gridrows[i].cells[2].style.display = "";
                gridrows[i].cells[3].style.display = "";
                gridrows[i].cells[4].style.display = "none";

            }


            $('#MainContent_gvAdministradores').each(function () {
                $(this).find('tr').each(function () {
                    var admin = $(this).find("select option:selected").text();

                    if (admin == "Revisor" || admin == "Coordinador") {
                        $(this).css("display", "none");
                    }
                    else if (admin == "Administrador") {
                        $(this).css("display", "");
                    }

                });
            });
            return false;
        }

        function esconderColumnasRevisor() {
            var gridrows = document.getElementById('MainContent_gvAdministradores').rows;
            debugger;
            for (i = 0; i < gridrows.length; i++) {
                gridrows[i].cells[1].style.display = "none";
                gridrows[i].cells[2].style.display = "none";
                gridrows[i].cells[3].style.display = "none";
                if (gridrows[i].cells[4].style.display == "none") {
                    gridrows[i].cells[4].style.display = "";

                }
            }

            $('#MainContent_gvAdministradores').each(function () {
                $(this).find('tr').each(function () {
                    var admin = $(this).find("select option:selected").text();

                    if (admin == "Administrador" || admin == "Coordinador") {
                        $(this).css("display", "none");
                    }
                    else if (admin == "Revisor") {
                        $(this).css("display", "");
                    }

                });
            });
            return false;
        }
    </script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li class="active"><a href="ConfigurarAdministradores.aspx">Usuarios</a></li>
            <li><a href="Configurar_Semestres.aspx">Semestres</a></li>
            <li><a href="Indicadores.aspx">Indicadores</a></li>
            <li><a href="Carreras.aspx">Carreras</a></li>
            <li id="confEmail" runat="server" visible="false"><a href="Configurar_Correo.aspx">Correo</a></li>
        </ul>
    </div>

    <div class="col-md-9 space-up">
        <div id="paneMensajes">
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>
            </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid ">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Menu</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li id="tipoAdmin" onclick="selectorAdministrador(0); return false;" class="active" data-trigger="focus" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Administrador <span class="sr-only">(current)</span></a></li>
                        <li id="tipoRev" onclick="selectorRevisor(0); return false;" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Revisor</a></li>
                        <li id="tipoCor" onclick="selectorCoordinador(0); return false;" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Coordinador</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Registrar <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a onclick="mostrarFormulario(); selectorAdministrador(0); ">Administrador</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a onclick="mostrarFormulario(); selectorRevisor(0); return false;">Revisor</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a onclick="mostrarFormulario(); selectorCoordinador(0); return false;">Coordinador</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="form-inline">
                        <div class="form-group navbar-form navbar-left" role="search">
                            <!-- <input type="text" id="buscar" class="form-control" placeholder="Buscar Administrador">  -->
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TextBox ID="buscar" CssClass="form-control" AutoPostBack="true" OnTextChanged="buscar_TextChanged" placeholder="Buscar Administrador" runat="server"></asp:TextBox>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="buscar" EventName="TextChanged" />

                                </Triggers>

                            </asp:UpdatePanel>

                        </div>
                        <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" OnClientClick="inhabilitarRequest();" class="btn btn-default centrar" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Aviso!" data-content="La busqueda se realizara de acuerdo al tipo de usuario seleccionado" runat="server">Buscar</asp:LinkButton>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>


        <div id="formulario" style="display: none;" class="form-horizontal">
            <fieldset>
                <!-- Form Name -->

                <legend id="tipoUsuario">Registro de Administrador</legend>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Nombre Completo</label>
                    <div class="col-md-4">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="tbNombre1" runat="server" placeholder="Nombre Completo" CssClass="form-control input-md"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator1"
                                    runat="server"
                                    ErrorMessage="Digite nombre" ControlToValidate="tbNombre1"
                                    Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>


                <!-- Text input-->
                <div id="divCarne" style="display: none;" class="form-group">
                    <label class="col-md-4 control-label" id="lblCarne" for="textinput">Carne</label>
                    <div class="col-md-4">


                        <asp:UpdatePanel ID="UpdatePanel2" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="tbCarne1" runat="server" OnTextChanged="tbCarne1_TextChanged" AutoPostBack="true" TextMode="Number" CssClass="form-control input-md" placeholder="Carne"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator2"
                                    runat="server"
                                    ErrorMessage="Digite carne" ControlToValidate="tbCarne1"
                                    Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="tbCarne1" EventName="TextChanged" />
                            </Triggers>

                        </asp:UpdatePanel>
                    </div>
                </div>

                <div id="divEmail" style="display: block;">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Email</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="tbEmail1" TextMode="Email" runat="server" placeholder="Email" CssClass="form-control input-md"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                runat="server"
                                ErrorMessage="Digite email" Enabled="false" ControlToValidate="tbEmail1"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                </div>


                <!-- Text input-->
                <div id="divTelefono" class="form-group">
                    <label class="col-md-4 control-label" id="lblTelefono" for="textinput">Telefono</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbTelefono1" runat="server" placeholder="Teléfono" CssClass="form-control input-md"></asp:TextBox>

                    </div>
                </div>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Usuario</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbUsuario1" runat="server" placeholder="Usuario" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                            runat="server"
                            ErrorMessage="Digite usuario" Enabled="false" ControlToValidate="tbUsuario1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>


                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Contraseña</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbContrasena1" TextMode="Password" runat="server" placeholder="Contrasena" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                            runat="server"
                            ErrorMessage="Contraseña debe tener entre 6 y 10 digitos válidos (números y letras)." Enabled="false" ControlToValidate="tbContrasena1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Repetir Contraseña</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbRepetirContrasena1" TextMode="Password" runat="server" placeholder="Repetir Contraseña" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                            runat="server"
                            ErrorMessage="Digite repetir contraseña" Enabled="false" ControlToValidate="tbRepetirContrasena1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <div class="col-md-6">
                            <%--OnClientClick="SetDataField(document.getElementById('<%=HiddenField1.ClientID %>').value);"--%>
                            <asp:LinkButton ID="Button1" OnClick="Button1_Click" CssClass="btn btn-primary" runat="server" Text="Registrar" />

                        </div>
                        <div class="col-md-6">
                            <button id="singlebutton1" name="singlebutton" onclick="ocultarFormulario(); return false;" class="btn btn-default">Cancelar</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>




        <div id="tabla" style="display: none;">
            <div class="table-responsive">
                <table class="table">

                    <asp:GridView ID="gvAdministradores" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                        DataKeyNames="ID_USUARIO"
                        OnRowCommand="GridView1_RowCommand">

                        <Columns>
                            <asp:TemplateField HeaderText="Tipo">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="form-control col-sm-4" Enabled="false" Width="145" OnDataBinding="ddAdministradores_DataBinding" ID="ddlTipo_usuarios" runat="server">
                                        <asp:ListItem Value="1">Administrador</asp:ListItem>
                                        <asp:ListItem Value="2">Revisor</asp:ListItem>
                                        <asp:ListItem Value="3">Coordinador</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NOMBRE">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbNom" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("NOMBRE") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TELEFONO">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbTel" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("TELEFONO") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EMAIL">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbCor" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("EMAIL") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NOMBRE_REVISOR">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbNomRevisor" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("NOMBRE_REVISOR") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="USUARIO">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbUsu" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("USUARIO") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CLAVE">
                                <ItemTemplate>
                                    <asp:TextBox ID="tbCla" runat="server" ClientIDMode="AutoID"
                                        Text='<%# Eval("CLAVE") %>' Enabled="false" Width="120" CssClass="form-control col-sm-4">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="editar" OnClientClick="inhabilitarRequest();" runat="server"
                                        CommandName="Editar"
                                        CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                        Text="<span class='glyphicon glyphicon-edit'></span> Editar"
                                        CssClass="btn btn-warning"
                                        ClientIDMode="AutoID" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Inabilitar_button" OnClientClick="inhabilitarRequest();"  runat="server"
                                        CommandName="Inabilitar"
                                        CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                        Text="<span class='glyphicon glyphicon-edit'></span> Inhabilitar"
                                        CssClass="btn btn-warning"
                                        ClientIDMode="AutoID" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </table>
            </div>
        </div>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
